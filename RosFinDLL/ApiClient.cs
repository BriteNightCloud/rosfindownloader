﻿using Newtonsoft.Json;
using RosFinDLL.Responses;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace RosFinDLL
{
    public sealed class ApiClient
    {
        private string apiAddress;
        private string accessToken;
        private X509Certificate2 certificate;

        /// <summary>
        /// Creates an instance of an API client.
        /// </summary>
        /// <param name="userName">Username used for login to the website.</param>
        /// <param name="password">Password used for login to the website.</param>
        /// <param name="apiAddress">Base address of API (ex https://api.website.com:8080/api-v2/services).</param>
        /// <param name="certificate">CSP certificate used for login to the website.</param>
        public ApiClient(string userName, string password, string apiAddress, X509Certificate2 certificate)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            this.apiAddress = apiAddress;
            this.certificate = certificate;

            accessToken = GetAccessToken(userName, password)?.value?.accessToken;
        }

        /// <summary>
        /// Get an access token that'll be used later in each api call.
        /// </summary>
        /// <returns>An instance of AccessTokenResponse class.</returns>
        /// <exception cref="HttpResponseStatusException">Http response status code wasn't 200 OK.</exception>
        private AccessTokenResponse GetAccessToken(string userName, string password)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{apiAddress}/authenticate");
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ClientCertificates.Add(certificate);
            request.Method = "POST";
            request.ContentType = "application/json";

            string postData = $"{{ \"userName\": \"{userName}\", \"password\": \"{password}\" }}";
            byte[] data = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            using (var sr = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                AccessTokenResponse response = JsonConvert.DeserializeObject<AccessTokenResponse>(sr.ReadToEnd());
                return response;
            }
        }

        /// <summary>
        /// Get an information of current catalog.
        /// </summary>
        /// <param name="apiPath">Accepts a path to an API method (ex 'suspect-catalogs/current-te21-catalog').</param>
        /// <returns>An instance of CurrentCatalogResponse class.</returns>
        /// <exception cref="HttpResponseStatusException">Http response status code wasn't 200 OK.</exception>
        public CurrentCatalogResponse GetCurrentCatalog(string apiPath)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{apiAddress}/{apiPath}");
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ClientCertificates.Add(certificate);
            request.Method = "POST";
            request.ContentLength = 0;
            request.PreAuthenticate = true;
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            using (var sr = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                CurrentCatalogResponse response = JsonConvert.DeserializeObject<CurrentCatalogResponse>(sr.ReadToEnd());
                return response;
            }
        }

        /// <summary>
        /// Gets current file by its ID.
        /// </summary>
        /// <param name="apiPath">Accepts a path to an API method (ex 'suspect-catalogs/current-te21-file').</param>
        /// <param name="catalogId">An ID of the current file.</param>
        /// <returns>A key-value pair where the key is filename and the value is bytes of file.</returns>
        public KeyValuePair<string, byte[]> GetCurrentFile(string apiPath, string catalogId)
        {
            FormUrlEncodedContent content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("id", catalogId) });

            var handler = new HttpClientHandler();
            handler.ClientCertificates.Add(certificate);
            handler.Credentials = CredentialCache.DefaultCredentials;

            HttpClient httpClient = new HttpClient(handler);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            HttpResponseMessage httpResponse = httpClient.PostAsync($"{apiAddress}/{apiPath}", content).Result;

            string filename = httpResponse.Content.Headers.ContentDisposition.FileNameStar;
            byte[] filedata = httpResponse.Content.ReadAsByteArrayAsync().Result;

            return new KeyValuePair<string, byte[]>(filename, filedata);
        }
    }
}
