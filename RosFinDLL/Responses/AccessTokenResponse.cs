﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosFinDLL.Responses
{
    public class AccessTokenResponse
    {
        public Value value { get; set; }
        public bool success { get; set; }
        public object error { get; set; }
        public object[] errors { get; set; }
        public bool hasErrors { get; set; }
    }

    public class Value
    {
        public Currentuser currentUser { get; set; }
        public string accessToken { get; set; }
        public object refreshToken { get; set; }
    }

    public class Currentuser
    {
        public string id { get; set; }
        public string userName { get; set; }
        public object kbShortName { get; set; }
        public int kbLoginType { get; set; }
        public bool isAuthenticated { get; set; }
    }

}
