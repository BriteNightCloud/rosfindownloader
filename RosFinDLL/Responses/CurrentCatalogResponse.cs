﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosFinDLL.Responses
{ 
    public class CurrentCatalogResponse
    {
        public DateTime date { get; set; }
        public bool isActive { get; set; }
        public string idXml { get; set; }
    }

}
