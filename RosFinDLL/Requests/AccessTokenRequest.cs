﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosFinDLL.Requests
{
    public sealed class AccessTokenRequest
    {
        public string userName { get; set; }
        public string password { get; set; }

        public AccessTokenRequest(string userName, string password)
        {
            this.userName = userName;
            this.password = password;
        }
    }
}
