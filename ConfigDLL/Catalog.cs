﻿using System;

namespace ConfigDLL
{
    public sealed class Catalog
    {
        public string Name { get; set; }
        public string CatalogApiMethod { get; set; }
        public string FileApiMethod { get; set; }
        public DateTime LatestDownload { get; set; }
        public bool IsSentToServer { get; set; }
    }
}
