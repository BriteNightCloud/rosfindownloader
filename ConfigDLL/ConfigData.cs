﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System.Diagnostics;

namespace ConfigDLL
{
    public static class ConfigData
    {
        private const int DEFAULT_PORT = 4600;

        private static RegistryKey registryKeyValue;

        private static RegistryKey RegistryKey
        {
            get
            {
                return registryKeyValue != null ? 
                    registryKeyValue = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\RosFinAPI", true) : 
                    Registry.LocalMachine.CreateSubKey(@"SOFTWARE\RosFinAPI");
            }
        }

        public static string ApiAddress
        {
            get { return (string) RegistryKey.GetValue("API_Address"); }
            set { RegistryKey.SetValue("API_Address", value); }
        }

        public static string Login
        {
            get { return (string)RegistryKey.GetValue("Login"); }
            set { RegistryKey.SetValue("Login", value); }
        }

        public static string Password
        {
            get { return (string)RegistryKey.GetValue("Password"); }
            set { RegistryKey.SetValue("Password", value); }
        }

        public static string CertThumbprint
        {
            get { return (string)RegistryKey.GetValue("Thumbprint"); }
            set { RegistryKey.SetValue("Thumbprint", value); }
        }

        public static string ServerAddress
        {
            get { return (string)RegistryKey.GetValue("Server_Address"); }
            set { RegistryKey.SetValue("Server_Address", value); }
        }

        public static int ServerPort
        {
            get
            {
                int port;
                int.TryParse(RegistryKey.GetValue("Server_Port").ToString(), out port);
                return port;
            }
            set { RegistryKey.SetValue("Server_Port", value); }
        }

        public static Catalog[] Catalogs
        {
            get { return JsonConvert.DeserializeObject<Catalog[]>((string) RegistryKey.GetValue("Catalogs")); }
            set { RegistryKey.SetValue("Catalogs", JsonConvert.SerializeObject(value)); }
        }

        /// <summary>
        /// Initializes config keys in registry.
        /// </summary>
        /// <returns>Returns false if the config is already exists and returns true otherwise.</returns>
        public static bool InitConfig()
        {
            if (ApiAddress != null
                && Login != null
                && Password != null
                && CertThumbprint != null
                && Catalogs != null
                && ServerAddress != null)
            {
                return false;
            }

            RegistryKey.SetValue("API_Address", "");

            RegistryKey.SetValue("Login", "");
            RegistryKey.SetValue("Password", "");
            RegistryKey.SetValue("Thumbprint", "");

            RegistryKey.SetValue("Server_Address", "");
            RegistryKey.SetValue("Server_Port", DEFAULT_PORT);

            RegistryKey.SetValue("Catalogs", "");

            EventLog.WriteEntry("RosFinAPI", "Конфиг не найден. Были созданы необходимые записи в реестре.\n" +
                @"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\RosFinAPI", EventLogEntryType.Information);

            return true;
        }
    }
}
